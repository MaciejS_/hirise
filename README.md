# HiRISE

# What is it?
This repository contains experimental modifications of RISE and experimentation tools created for the needs of my Master's thesis.

The `hirise` submodule consists of three modifications:
* HiRISE - High-resolution RISE - an abandoned attempt at iterative refinement of saliency maps (succesfully implemented by [HiPe](https://arxiv.org/abs/2103.05108))
* VRISE - Voronoi-RISE - this modification relaxes a constraint placed on occlusion shapes by RISE and replaces occlusions sampled from a square grid with convex polygons sampled from a Voronoi mesh.
* Occlusion generators with "informativeness guarantee" - implemented as part of VRISE (although also applicable to RISE) - informativeness guarantee ensures that occlusion rate of an individual occlusion pattern is never 0% or 100%, as both un-occluded and fully occluded images burn GPU time without adding any new information to the final saliency map


# Contents 
* `demo` - demonstration Jupyter notebooks
* `hirise` - core code components
* `hirise_tools` - experimentation tools (for non-interactive, computation-heavy experiments)
* `hirise_nbutils` - convenience functions and helper code for Jupyter notebooks

# Getting started
1. Set up a virtual environment accoriding to instructions in `hirise/README.md`
2. Start Jupyter in this directory
3. Run any of the notebooks from the `demo/` directory

# Testing
* Run `run_tests.sh` to automatically test all modules
* Run `python3 -m unittest -s $modulename` from this directory to run tests for individual modules
Caveat! Some tests in `hirise` package are probabilistic and may fail in rare circumstances. If 1-2 tests in the suite fail, re-run the suite. Unless failures persist, the code is working correcly.

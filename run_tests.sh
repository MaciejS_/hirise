#!/bin/bash

declare -a modules=("hirise" "hirise_tools" "hirise_nbutils")
n_modules=${#modules[@]}

for i in $(seq 1 $n_modules); do
    tested_module=${modules[$((i-1))]}
    #echo ">${tested_module}<"
    python3 -m unittest discover -s $tested_module
    echo "Tests for $tested_module finished ($i of $n_modules modules) "
    sleep 1
done
